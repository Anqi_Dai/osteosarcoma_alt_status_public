#!/usr/bin/env python
'''
Usage:
    clean_quant_genes_sf_file.py -o OUTPUT  INPUT

Options:
    -o       output file 

This script will work on:
remove the .XX in the geneIDs of the Name column
drop duplicates if any
output only Name and NumReads columns with no header 
'''

import pandas as pd
from docopt import docopt

options = docopt(__doc__)
output = options['OUTPUT']
input = options['INPUT']


# remove the . and the numbers after the . in the Name
# then drop duplicates
DF = pd.read_csv(input, sep='\t')
DF["Name"]=DF["Name"].apply(lambda el: el.split('.')[0])
DF=DF.drop_duplicates(subset = 'Name')

# output only two columns
DF= DF[['Name','NumReads']]

# output to tsv with .2f
DF.to_csv(output, sep='\t', index=False, header=None, float_format='%.2f')
