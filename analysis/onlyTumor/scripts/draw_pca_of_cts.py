#!/usr/bin/env python

'''
Usage: draw_pca_of_cts.py -o OUTPUT INPUT

Options:
    -o     output pca plot

This script will draw PCA plot of the input counts matrix.
'''

import matplotlib
matplotlib.use('agg')
import pandas as pd
from docopt import docopt
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

options = docopt(__doc__)
input = options['INPUT']
output = options['OUTPUT']

# input is the 
sig_data = input
df = pd.read_csv(sig_data, index_col=0)

# transpose to get the samples' projection
df = df.T
# this seems to be an outlier sample
df = df[df.index!="SRR1701812"]
pca = PCA(n_components=2)
res = pca.fit(df)
res_r = pca.fit(df).transform(df)

plt.figure(figsize=(8,8))
plt.scatter(res_r[:,0], res_r[:,1], alpha=0.4)
plt.title('PCA of the samples')
plt.xlabel('PC1 explains {:.02f}%'.format(pca.explained_variance_ratio_[0]*100))
plt.ylabel('PC2 explains {:.02f}%'.format(pca.explained_variance_ratio_[1]*100))

# output to a png file
plt.savefig(output)
