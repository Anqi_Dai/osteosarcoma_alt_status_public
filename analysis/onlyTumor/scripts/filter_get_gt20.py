#!/usr/bin/env python
'''
Usage: filter_get_gt20.py -o OUTPUT INPUT

Options:
    -o      output of the filtered matrix

This script will filter to get the genes with average count greater than 20 accross the samples, since that number leaves 19000 + genes left.
'''

import pandas as pd
from docopt import docopt
import numpy

options = docopt(__doc__)
input = options['INPUT']
output = options['OUTPUT']


df = pd.read_csv(input).set_index('GeneID')
df.index.name = None 

# try several filtering threshold. The goal is to have 19000 + genes left,
# since that is about the number of genes with sufficient abundance.

possible_filter = []
for num in range(10,60,10):
    df_fil = df[df.apply(numpy.average, axis=1)> num]
    possible_filter.append([num,len(df_fil.index) ])

num = [_[0] for _ in possible_filter if _[1] > 19000 and _[1] < 20000][0]

df = df[df.apply(numpy.average, axis=1)> num]

df.to_csv(output,  float_format='%.2f')
