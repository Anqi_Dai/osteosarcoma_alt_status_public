#!/usr/bin/env python
'''
Usage: select_CL_sig_genes.py -o OUTPUT -g GENEID -i INPUT

Options: 
  -o     output subsetted counts matrix
  -g     FDR01 or FDR05 genes
  -i     normalized filtered counts matrix

This script will select the cell line FDR 05 or FDR 01 significant genes from 
the normalized filtered counts matrix of the new tumor samples. 
'''

import pandas as pd
from docopt import docopt

options = docopt(__doc__)
input = options['INPUT']
output = options['OUTPUT']
geneID = options['GENEID']

sig_genes = pd.read_csv(geneID)[['ensembl_gene_id']]

df = pd.read_csv(input, index_col=0)

sig_genes_tumor = sig_genes.join(df, on = 'ensembl_gene_id', how = 'inner')

sig_genes_tumor.to_csv(output, index = False)

